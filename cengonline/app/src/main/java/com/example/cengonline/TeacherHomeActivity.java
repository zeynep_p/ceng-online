package com.example.cengonline;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class TeacherHomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_home);
    }
    public void fn(View view) {
        if(view.getId()==R.id.addStudentButton){
            // Toast.makeText(MainActivity.this, "Hello", Toast.LENGTH_SHORT).show();
            Intent intent3=new Intent(TeacherHomeActivity.this, CrudEnrollStudentOperation.class);
            startActivity(intent3);
        }
        else if(view.getId()==R.id.teacherButton){
            // Toast.makeText(MainActivity.this, "Hello", Toast.LENGTH_SHORT).show();
            Intent intent=new Intent(TeacherHomeActivity.this,CrudOperation.class);
            startActivity(intent);
        }
        else if(view.getId()==R.id.submittedWorkButton){
            // Toast.makeText(MainActivity.this, "Hello", Toast.LENGTH_SHORT).show();
            Intent intent=new Intent(TeacherHomeActivity.this,EnrolledCourseTeacherView.class);
            startActivity(intent);
        }
        else if(view.getId()==R.id.chatButton){
            // Toast.makeText(MainActivity.this, "Hello", Toast.LENGTH_SHORT).show();
            Intent intent=new Intent(TeacherHomeActivity.this,TeacherChatActivity.class);
            startActivity(intent);
        }
        else if(view.getId()==R.id.postButton){
            // Toast.makeText(MainActivity.this, "Hello", Toast.LENGTH_SHORT).show();
            Intent intent=new Intent(TeacherHomeActivity.this,CrudCoursePostOperation.class);
            startActivity(intent);
        }
        else{
            Intent intent2=new Intent(TeacherHomeActivity.this,CrudAnnouncementOperation.class);
            startActivity(intent2);
        }
    }

    private class MyListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        }
    }
}
