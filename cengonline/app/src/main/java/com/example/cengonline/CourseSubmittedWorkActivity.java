package com.example.cengonline;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CourseSubmittedWorkActivity extends AppCompatActivity {
    public static final String SUBMITTEDWORK_NAME = "com.example.cengonline.submittedworkname";
    public static final String SUBMITTEDWORK_ID = "com.example.cengonline.submittedworkid";


    Button buttonAddSubmittedWork;
    EditText editTextSubmittedWorkName;
    EditText editTextSubmittedWorkInfo;
    TextView  textViewCourse;
    ListView listViewSubmittedWorks;

    DatabaseReference databaseSubmittedWorks;

    List<SubmittedWork> submittedworks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_submitted_work);

        Intent intent = getIntent();

        /*
         * this line is important
         * this time we are not getting the reference of a direct node
         * but inside the node submittedwork we are creating a new child with the course id
         * and inside that node we will store all the submittedworks with unique ids
         * */
        databaseSubmittedWorks = FirebaseDatabase.getInstance().getReference("submittedworks").child(intent.getStringExtra(EnrolledCourseAssignment.ASSIGNMENT_ID));

        buttonAddSubmittedWork = (Button) findViewById(R.id.buttonAddSubmittedWork);
        editTextSubmittedWorkName = (EditText) findViewById(R.id.editTextName);
        editTextSubmittedWorkInfo = (EditText) findViewById(R.id.editTextInfo);
        textViewCourse = (TextView) findViewById(R.id.textViewCourse);
        listViewSubmittedWorks = (ListView) findViewById(R.id.listViewSubmittedWorks);

        submittedworks = new ArrayList<>();

        textViewCourse.setText(intent.getStringExtra(CrudOperation.COURSE_NAME));


        buttonAddSubmittedWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveSubmittedWork();
            }
        });

        listViewSubmittedWorks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected submittedwork
                SubmittedWork submittedwork = submittedworks.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), CourseSubmittedWorkActivity.class);

                //putting submittedwork name and id to intent
                intent.putExtra(SUBMITTEDWORK_ID, submittedwork.getId());
                intent.putExtra(SUBMITTEDWORK_NAME, submittedwork.getName());

                //starting the activity with intent
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseSubmittedWorks.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                submittedworks.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    SubmittedWork submittedwork = postSnapshot.getValue(SubmittedWork.class);
                    submittedworks.add(submittedwork);
                }
                SubmittedWorkList submittedworkListAdapter = new SubmittedWorkList(CourseSubmittedWorkActivity.this, submittedworks);
                listViewSubmittedWorks.setAdapter(submittedworkListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void saveSubmittedWork() {
        String submittedworkName = editTextSubmittedWorkName.getText().toString().trim();
        String submittedworkInfo = editTextSubmittedWorkInfo.getText().toString().trim();
        if (!TextUtils.isEmpty(submittedworkName)) {
            String id  = databaseSubmittedWorks.push().getKey();
            SubmittedWork submittedwork = new SubmittedWork(id, submittedworkName, submittedworkInfo);
            databaseSubmittedWorks.child(id).setValue(submittedwork);
            Toast.makeText(this, "SubmittedWork saved", Toast.LENGTH_LONG).show();
            editTextSubmittedWorkName.setText("");
        } else {
            Toast.makeText(this, "Please enter submittedwork name", Toast.LENGTH_LONG).show();
        }
    }





















}