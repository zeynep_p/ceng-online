package com.example.cengonline;

import com.google.firebase.database.IgnoreExtraProperties;
@IgnoreExtraProperties
public class Assignment extends ModuleInformation {
    private String dueDate;

    // CONSTRUCTORS
    public Assignment(String id, String name, String info, String dueDate) {
        super(id, name, info);
        this.dueDate=dueDate;
    }
    public Assignment() {
    }

    public Assignment(String id, String name) {
        super(id, name);
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }


    @Override
    public String toString() {
        return "Assignment{" + super.toString()+
                "dueDate='" + dueDate + '\'' +
                '}';
    }
}
