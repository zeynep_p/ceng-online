package com.example.cengonline;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CrudEnrollStudentOperation extends AppCompatActivity {

    //we will use these constants later to pass the course name and id to another activity
    public static final String COURSE_NAME = "com.example.cengonline.coursename";
    public static final String COURSE_ID = "com.example.cengonline.courseid";

    //view objects
    EditText editTextName;
    Spinner spinnerInfo;
    Button buttonAddCourse;
    ListView listViewCourses;

    //a list to store all the course from firebase database
    List<Course> courses;

    //our database reference object
    DatabaseReference databaseCourses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crud_enroll_operation);

        //getting the reference of courses node
        databaseCourses = FirebaseDatabase.getInstance().getReference("courses");

        //getting views
        editTextName = (EditText) findViewById(R.id.editTextName);
        spinnerInfo = (Spinner) findViewById(R.id.spinnerInfos);
        listViewCourses = (ListView) findViewById(R.id.listViewCourses);

        buttonAddCourse = (Button) findViewById(R.id.buttonAddCourse);

        //list to store courses
        courses = new ArrayList<>();


        //adding an onclicklistener to button
        buttonAddCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //calling the method addCourse()
                //the method is defined below
                //this method is actually performing the write operation
                addCourse();
            }
        });

        listViewCourses.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected course
                Course course = courses.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), EnrollStudentActivity.class);

                //putting course name and id to intent
                intent.putExtra(COURSE_ID, course.getId());
                intent.putExtra(COURSE_NAME, course.getName());

                //starting the activity with intent
                startActivity(intent);
            }
        });

        listViewCourses.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Course course = courses.get(i);
                showUpdateDeleteDialog(course.getId(), course.getName());
                return true;
            }
        });
    }

    /*
     * This method is saving a new course to the
     * Firebase Realtime Database
     * */
    private void addCourse() {
        //getting the values to save
        String name = editTextName.getText().toString().trim();
        String info = spinnerInfo.getSelectedItem().toString();

        //checking if the value is provided
        if (!TextUtils.isEmpty(name)) {

            //getting a unique id using push().getKey() method
            //it will create a unique id and we will use it as the Primary Key for our Course
            String id = databaseCourses.push().getKey();

            //creating an Course Object
            Course course = new Course(id, name, info);

            //Saving the Course
            databaseCourses.child(id).setValue(course);

            //setting edittext to blank again
            editTextName.setText("");

            //displaying a success toast
            Toast.makeText(this, "Course added", Toast.LENGTH_LONG).show();
        } else {
            //if the value is not given displaying a toast
            Toast.makeText(this, "Please enter a name", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        //attaching value event listener
        databaseCourses.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //clearing the previous course list
                courses.clear();

                //iterating through all the nodes
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //getting course
                    Course course = postSnapshot.getValue(Course.class);
                    //adding course to the list
                    courses.add(course);
                }

                //creating adapter
                CourseList courseAdapter = new CourseList(CrudEnrollStudentOperation.this, courses);
                //attaching adapter to the listview
                listViewCourses.setAdapter(courseAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showUpdateDeleteDialog(final String courseId, String courseName) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextName = (EditText) dialogView.findViewById(R.id.editTextName);
        final Spinner spinnerInfo = (Spinner) dialogView.findViewById(R.id.spinnerInfos);
        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.buttonUpdateCourse);
        final Button buttonDelete = (Button) dialogView.findViewById(R.id.buttonDeleteCourse);

        dialogBuilder.setTitle(courseName);
        final AlertDialog b = dialogBuilder.create();
        b.show();


        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editTextName.getText().toString().trim();
                String info = spinnerInfo.getSelectedItem().toString();
                if (!TextUtils.isEmpty(name)) {
                    updateCourse(courseId, name, info);
                    b.dismiss();
                }
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteCourse(courseId);
                b.dismiss();
            }
        });
    }


    private boolean updateCourse(String id, String name, String info) {
        //getting the specified course reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("courses").child(id);

        //updating course
        Course course = new Course(id, name, info);
        dR.setValue(course);
        Toast.makeText(getApplicationContext(), "Course Updated", Toast.LENGTH_LONG).show();
        return true;
    }


    private boolean deleteCourse(String id) {
        //getting the specified course reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("courses").child(id);

        //removing course
        dR.removeValue();

        //getting the enrollments reference for the specified course
        DatabaseReference drEnrollments = FirebaseDatabase.getInstance().getReference("students_enrollments").child(id);

        //removing all enrollments
        drEnrollments.removeValue();
        Toast.makeText(getApplicationContext(), "Course Deleted", Toast.LENGTH_LONG).show();

        return true;
    }
}