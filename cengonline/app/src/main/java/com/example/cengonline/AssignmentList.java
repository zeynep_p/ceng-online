package com.example.cengonline;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class AssignmentList extends ArrayAdapter<Assignment> {
    private Activity context;
    List<Assignment> assignments;

    public AssignmentList(Activity context, List<Assignment> assignments) {
        super(context, R.layout.layout_assignment_list, assignments);
        this.context = context;
        this.assignments = assignments;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_assignment_list, null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewInfo = (TextView) listViewItem.findViewById(R.id.textViewInfo);
        TextView textViewDueDate = (TextView) listViewItem.findViewById(R.id.textViewDueDate);

        Assignment assignment = assignments.get(position);
        textViewName.setText(assignment.getName());
        textViewInfo.setText(assignment.getInfo());
        textViewDueDate.setText(assignment.getDueDate());

        return listViewItem;
    }
}