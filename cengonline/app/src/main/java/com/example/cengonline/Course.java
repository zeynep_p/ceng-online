package com.example.cengonline;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Course extends ModuleInformation {
    // CONSTRUCTORS
    public Course(String id, String name, String info) {
        super(id, name, info);
    }

    public Course() {
    }

    public Course(String id, String name) {
        super(id, name);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}







