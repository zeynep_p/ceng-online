package com.example.cengonline;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CourseStudentPostActivity extends AppCompatActivity {

    public static final String POST_NAME = "com.example.cengonline.postname";
    public static final String POST_ID = "com.example.cengonline.postid";


    TextView  textViewCourse;
    ListView listViewPosts;

    DatabaseReference databasePosts;

    List<Post> posts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_student_post);

        Intent intent = getIntent();

        /*
         * this line is important
         * this time we are not getting the reference of a direct node
         * but inside the node post we are creating a new child with the course id
         * and inside that node we will store all the posts with unique ids
         * */
        databasePosts = FirebaseDatabase.getInstance().getReference("posts").child(intent.getStringExtra(CrudCourseStudentPostOperation.COURSE_ID));



        textViewCourse = (TextView) findViewById(R.id.textViewCourse);
        listViewPosts = (ListView) findViewById(R.id.listViewPosts);

        posts = new ArrayList<>();

        textViewCourse.setText(intent.getStringExtra(CrudCoursePostOperation.COURSE_NAME));




        listViewPosts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected post
                Post post = posts.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), CourseStudentCommentActivity.class);

                //putting post name and id to intent
                intent.putExtra(POST_ID, post.getId());
                intent.putExtra(POST_NAME , post.getName());

                //starting the activity with intent
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();

        databasePosts.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                posts.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Post post = postSnapshot.getValue(Post.class);
                    posts.add(post);
                }
                PostList postListAdapter = new PostList(CourseStudentPostActivity.this, posts);
                listViewPosts.setAdapter(postListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }













}