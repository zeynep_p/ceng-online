package com.example.cengonline;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;

public class EnrollStudentActivity extends AppCompatActivity {

    //we will use these constants later to pass the course name and id to another activity
    public static final String ENROLLSTUDENT_NAME = "com.example.cengonline.students_enrollmentsfullname";
    public static final String ENROLLSTUDENT_ID = "com.example.cengonline.students_enrollmentsid";
    public static final String ENROLLSTUDENT_EMAIL = "com.example.cengonline.students_enrollmentsemail";


    Button buttonAddStudent;
    EditText editTextStudentEmail, editTextStudentName;
    TextView  textViewCourse;
    ListView listViewStudents;

    DatabaseReference databaseStudentsEnrollments;

    DatabaseReference databaseStudentsCourses;
    //our database reference object
    List<EnrolledStudent> students;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_has_student);

        Intent intent = getIntent();

        /*
         * this line is important
         * this time we are not getting the reference of a direct node
         * but inside the node student we are creating a new child with the course id
         * and inside that node we will store all the students with unique ids
         * */
        databaseStudentsEnrollments = FirebaseDatabase.getInstance().getReference("students_enrollments").child(intent.getStringExtra(CrudEnrollStudentOperation.COURSE_ID));




        buttonAddStudent = (Button) findViewById(R.id.buttonAddStudent);
        editTextStudentEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextStudentName = (EditText) findViewById(R.id.editTextName);
        textViewCourse = (TextView) findViewById(R.id.textViewCourse);
        listViewStudents = (ListView) findViewById(R.id.listViewStudents);

        students = new ArrayList<>();

        textViewCourse.setText(intent.getStringExtra(CrudEnrollStudentOperation.COURSE_NAME));

        buttonAddStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveStudent();
            }
        });

        listViewStudents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected student
               EnrolledStudent student = students.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), EnrollStudentActivity.class);

                //putting student name and id to intent
                intent.putExtra(ENROLLSTUDENT_ID, student.getId());
                intent.putExtra(ENROLLSTUDENT_NAME, student.getFullname());
                intent.putExtra(ENROLLSTUDENT_EMAIL, student.getEmail());

                //starting the activity with intent
                startActivity(intent);
            }
        });

        listViewStudents.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                EnrolledStudent student = students.get(i);
                showUpdateDeleteDialog(student.getId(), student.getFullname(), student.getEmail());
                return true;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseStudentsEnrollments.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                students.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    EnrolledStudent student = postSnapshot.getValue(EnrolledStudent.class);
                    students.add(student);
                }
                EnrolledStudentList studentListAdapter = new EnrolledStudentList(EnrollStudentActivity.this, students);
                listViewStudents.setAdapter(studentListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showUpdateDeleteDialog(final String studentId, String studentName,String studentEmail) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_enrollment_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextName = (EditText) dialogView.findViewById(R.id.editTextName);
        final EditText editTextEmail = (EditText) dialogView.findViewById(R.id.editTextEmail);
        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.buttonUpdateStudent);
        final Button buttonDelete = (Button) dialogView.findViewById(R.id.buttonDeleteStudent);

        dialogBuilder.setTitle(studentName);
        final AlertDialog b = dialogBuilder.create();
        b.show();


        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editTextName.getText().toString().trim();
                String email = editTextEmail.getText().toString().trim();
                if (!TextUtils.isEmpty(name)) {
                    updateStudent(studentId, name,email);
                    b.dismiss();
                }
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteStudent(studentId);
                b.dismiss();
            }
        });
    }

    private boolean updateStudent(String id, String name, String email) {
        Intent intent = getIntent();
        //getting the specified course reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("students_enrollments").child(intent.getStringExtra(CrudEnrollStudentOperation.COURSE_ID));

        DatabaseReference dRA = dR.child(id);

        //updating student
        EnrolledStudent student = new EnrolledStudent(id, name,email);
        dRA.setValue(student);
        Toast.makeText(getApplicationContext(), "Student Updated", Toast.LENGTH_LONG).show();
        return true;
    }

    private void saveStudent() {
        String studentName = editTextStudentName.getText().toString().trim();
        String studentEmail = editTextStudentEmail.getText().toString().trim();
        if (!TextUtils.isEmpty(studentName)&& !TextUtils.isEmpty(studentEmail)) {
            String id  = databaseStudentsEnrollments.push().getKey();
            EnrolledStudent student = new EnrolledStudent(id, studentName,studentEmail);
            databaseStudentsEnrollments.child(id).setValue(student);

            Intent intent = getIntent();
            databaseStudentsCourses = FirebaseDatabase.getInstance().getReference("students_courses").child(studentName);

            Intent intent2 = getIntent();
            Course course= new Course(intent.getStringExtra(CrudEnrollStudentOperation.COURSE_ID),intent2.getStringExtra(CrudEnrollStudentOperation.COURSE_NAME));
            databaseStudentsCourses.child(course.getId()).setValue(course);
            //.child(studentName).

            Toast.makeText(this, "Student saved", Toast.LENGTH_LONG).show();
            editTextStudentName.setText("");
            editTextStudentEmail.setText("");
        } else {
            Toast.makeText(this, "Please enter student name", Toast.LENGTH_LONG).show();
        }
    }

    private boolean deleteStudent(String id) {
        Intent intent = getIntent();
        //getting the specified course reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("students_enrollments").child(intent.getStringExtra(CrudEnrollStudentOperation.COURSE_ID));

        DatabaseReference dRA = dR.child(id);

        //removing course
        dRA.removeValue();

        Toast.makeText(getApplicationContext(), "Student Deleted", Toast.LENGTH_LONG).show();

        return true;
    }

    /*
    public String getCourseId(){
        StudentLogin st=new StudentLogin();
        if(editTextStudentEmail.equals(st.getEmail())){
            return CrudEnrollStudentOperation.COURSE_ID;
        }
        return"";
    }
    */

}