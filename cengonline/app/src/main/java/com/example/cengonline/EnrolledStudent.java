package com.example.cengonline;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class EnrolledStudent  {
    private String id;
    private String fullname;
    private String email;

    // CONSTRUCTORS
    public EnrolledStudent(String id, String fullname, String email) {
        super();
        this.id = id;
        this.fullname = fullname;
        this.email = email;
    }



    public EnrolledStudent() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", fullname='" + fullname + '\'' +
                '}';
    }
}
