package com.example.cengonline;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CoursePostActivity extends AppCompatActivity {

    public static final String POST_NAME = "com.example.cengonline.postname";
    public static final String POST_ID = "com.example.cengonline.postid";

    Button buttonAddPost;
    EditText editTextPostName, editTextPostInfo, editTextPostDueDate;

    TextView  textViewCourse;
    ListView listViewPosts;

    DatabaseReference databasePosts;

    List<Post> posts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_post);

        Intent intent = getIntent();

        /*
         * this line is important
         * this time we are not getting the reference of a direct node
         * but inside the node post we are creating a new child with the course id
         * and inside that node we will store all the posts with unique ids
         * */
        databasePosts = FirebaseDatabase.getInstance().getReference("posts").child(intent.getStringExtra(CrudCoursePostOperation.COURSE_ID));

        buttonAddPost = (Button) findViewById(R.id.buttonAddPost);
        editTextPostName = (EditText) findViewById(R.id.editTextName);
        editTextPostInfo = (EditText) findViewById(R.id.editTextInfo);
        editTextPostDueDate = (EditText) findViewById(R.id.editTextDueDate);

        textViewCourse = (TextView) findViewById(R.id.textViewCourse);
        listViewPosts = (ListView) findViewById(R.id.listViewPosts);

        posts = new ArrayList<>();

        textViewCourse.setText(intent.getStringExtra(CrudCoursePostOperation.COURSE_NAME));


        buttonAddPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savePost();
            }
        });

        listViewPosts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected post
                Post post = posts.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), CourseCommentActivity.class);

                //putting post name and id to intent
                intent.putExtra(POST_ID, post.getId());
                intent.putExtra(POST_NAME , post.getName());

                //starting the activity with intent
                startActivity(intent);
            }
        });

        listViewPosts.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Post post = posts.get(i);
                showUpdateDeleteDialog(post.getId(), post.getName());
                return true;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        databasePosts.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                posts.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Post post = postSnapshot.getValue(Post.class);
                    posts.add(post);
                }
                PostList postListAdapter = new PostList(CoursePostActivity.this, posts);
                listViewPosts.setAdapter(postListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void savePost() {
        String postName = editTextPostName.getText().toString().trim();
        String postInfo = editTextPostInfo.getText().toString().trim();
        String postDueDate = editTextPostDueDate.getText().toString().trim();

        if (!TextUtils.isEmpty(postName)) {
            String id  = databasePosts.push().getKey();
            Post post = new Post(id, postName, postInfo, postDueDate);
            databasePosts.child(id).setValue(post);
            Toast.makeText(this, "Post saved", Toast.LENGTH_LONG).show();
            editTextPostName.setText("");
        } else {
            Toast.makeText(this, "Please enter post name", Toast.LENGTH_LONG).show();
        }
    }


    private void showUpdateDeleteDialog(final String postId, String postName) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_post_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextName = (EditText) dialogView.findViewById(R.id.editTextName);
        final EditText editTextInfo = (EditText) dialogView.findViewById(R.id.editTextInfo);
        final EditText editTextDuedate = (EditText) dialogView.findViewById(R.id.editTextDueDate);
        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.buttonUpdatePost);
        final Button buttonDelete = (Button) dialogView.findViewById(R.id.buttonDeletePost);

        dialogBuilder.setTitle(postName);
        final AlertDialog b = dialogBuilder.create();
        b.show();


        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editTextName.getText().toString().trim();
                String a_info = editTextInfo.getText().toString().trim();
                String duedate = editTextDuedate.getText().toString().trim();
                if (!TextUtils.isEmpty(name)) {
                    updatePost(postId, name, a_info, duedate);
                    b.dismiss();
                }
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deletePost(postId);
                b.dismiss();
            }
        });
    }


    private boolean updatePost(String id, String name, String info, String duedate) {
        Intent intent = getIntent();
        //getting the specified post reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("posts").child(intent.getStringExtra(CrudCoursePostOperation.COURSE_ID));

        DatabaseReference dRA = dR.child(id);

        Post post = new Post(id, name, info, duedate);
        dRA.setValue(post);
        Toast.makeText(getApplicationContext(), "Post Updated", Toast.LENGTH_LONG).show();
        return true;

    }


    private boolean deletePost(String id) {
        Intent intent = getIntent();
        //getting the specified post reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("posts").child(intent.getStringExtra(CrudCoursePostOperation.COURSE_ID));

        DatabaseReference dRA = dR.child(id);

        //removing post
        dRA.removeValue();

        Toast.makeText(getApplicationContext(), "Post Deleted", Toast.LENGTH_LONG).show();

        return true;
    }









}