package com.example.cengonline;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class PostList extends ArrayAdapter<Post> {
    private Activity context;
    List<Post> posts;

    public PostList(Activity context, List<Post> posts) {
        super(context, R.layout.layout_post_list, posts);
        this.context = context;
        this.posts = posts;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_post_list, null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewInfo = (TextView) listViewItem.findViewById(R.id.textViewInfo);
        TextView textViewDueDate = (TextView) listViewItem.findViewById(R.id.textViewDueDate);

        Post post = posts.get(position);
        textViewName.setText(post.getName());
        textViewInfo.setText(post.getInfo());
        textViewDueDate.setText(post.getDueDate());

        return listViewItem;
    }
}