package com.example.cengonline;

import android.app.Activity;
//import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


public class CourseList extends ArrayAdapter<Course> {
    private Activity context;
    List<Course> courses;

    public CourseList(Activity context, List<Course> courses) {
        super(context, R.layout.layout_course_list, courses);
        this.context = context;
        this.courses = courses;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_course_list, null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewGenre = (TextView) listViewItem.findViewById(R.id.textViewInfo);

        Course course = courses.get(position);
        textViewName.setText(course.getName());
        textViewGenre.setText(course.getInfo());

        return listViewItem;
    }
}