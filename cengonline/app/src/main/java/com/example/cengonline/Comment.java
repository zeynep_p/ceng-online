package com.example.cengonline;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Comment extends ModuleInformation {
    // CONSTRUCTORS
    public Comment(String id, String name, String info) {
        super(id, name, info);
    }

    public Comment() {
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
