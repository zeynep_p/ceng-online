package com.example.cengonline;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class EnrolledCourseView extends AppCompatActivity {

    //we will use these constants later to pass the course name and id to another activity
    public static final String COURSE_NAME = "com.example.cengonline.coursename";
    public static final String COURSE_ID = "com.example.cengonline.courseid";

    //view objects

    ListView listViewCourses;

    //a list to store all the course from firebase database
    List<Course> courses;

    //our database reference object
    DatabaseReference databaseCourses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_enrolled_course);

        StudentLogin st=new StudentLogin();

        //getting the reference of courses node
        databaseCourses = FirebaseDatabase.getInstance().getReference("students_courses").child("zeynep");;

        //getting views

        listViewCourses = (ListView) findViewById(R.id.listViewCourses);


        //list to store courses
        courses = new ArrayList<>();



        listViewCourses.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected course
                Course course = courses.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), EnrolledCourseAssignment.class);

                //putting course name and id to intent
                intent.putExtra(COURSE_ID, course.getId());
                intent.putExtra(COURSE_NAME, course.getName());

                //starting the activity with intent
                startActivity(intent);
            }
        });


    }

    /*
     * This method is saving a new course to the
     * Firebase Realtime Database
     * */



    @Override
    protected void onStart() {
        super.onStart();
        //attaching value event listener
        databaseCourses.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //clearing the previous course list
                courses.clear();

                //iterating through all the nodes
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //getting course
                    Course course = postSnapshot.getValue(Course.class);
                    //adding course to the list
                    courses.add(course);
                }

                //creating adapter
                CourseList courseAdapter = new CourseList(EnrolledCourseView.this, courses);
                //attaching adapter to the listview
                listViewCourses.setAdapter(courseAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}