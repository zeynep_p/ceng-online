package com.example.cengonline;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class AnnouncementList extends ArrayAdapter<Announcement> {
    private Activity context;
    List<Announcement> announcements;

    public AnnouncementList(Activity context, List<Announcement> announcements) {
        super(context, R.layout.layout_course_list, announcements);
        this.context = context;
        this.announcements = announcements;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_course_list, null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewRating = (TextView) listViewItem.findViewById(R.id.textViewInfo);

        Announcement announcement = announcements.get(position);
        textViewName.setText(announcement.getName());
        textViewRating.setText(String.valueOf(announcement.getInfo()));

        return listViewItem;
    }
}