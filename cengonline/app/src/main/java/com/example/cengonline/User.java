package com.example.cengonline;

public interface User {

     public String  getId();

     public void setId(String id) ;

     public String  getFullname();

     public void setFullname(String fullname) ;

     public String  getEmail();

     public void setEmail(String email) ;

     public String  getPassword();

     public void setPassword(String password) ;

     public String  getPhone();

     public void setPhone(String phone) ;

}






